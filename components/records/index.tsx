'use client'
import { useState } from "react";
import { getRecords } from "../../endpoints/records";
import RecordForm from "src/recordForm";

interface Record {
    name: string;
    email: string;
    description: string;
}


const Records = () => {

    const [records, setRecords] = useState<Record[]>([]);

    const fetchRecords = async () => {
        try {
            const data : any = await getRecords();
            console.log({data})
            if (data.rows) {
                setRecords(data.rows);
            }
        } catch (err) {
            console.log("Error while fetching records : ", err)
        }
    }

    return (
        <div className="flex wrap gap-8 justify-center mt-16">
            <div className="bg-white rounded-md shadow-md px-8 py-4 h-[70vh] overflow-auto">
                <div className="mx-auto mt-8">
                    <div className="mx-auto mt-8">
                        <div className="flex justify-between items-center mb-4">
                            <h2
                                onClick={fetchRecords}
                                className="cursor-pointer text-xl bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600"
                            >
                                Fetch Records
                            </h2>
                        </div>
                        <div>
                            {records?.map(({ name, email, description }, index) => (
                                <div key={index} className="bg-white p-4 mb-4 border rounded-md shadow-md flex gap-4">
                                    <div className="min-w-40">
                                        <h3 className="text-xl font-semibold">{'Name'}</h3>
                                        <p className="text-gray-600">{name}</p>
                                    </div>
                                    <div className="min-w-40">
                                        <h3 className="text-xl font-semibold">{'Email'}</h3>
                                        <p className="text-gray-600">{email}</p>
                                    </div>

                                    <div className="max-w-40">
                                        <h3 className="text-xl font-semibold">{'Description'}</h3>
                                        <p className="text-gray-600">{description}</p>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            <div className="max-w-md bg-white rounded-md shadow-md px-8 py-4">
                <RecordForm />
            </div>
        </div>
    );
}

export default Records;
