'use server'
import { sql } from '@vercel/postgres';
import { unstable_noStore as noStore } from 'next/cache';

type IFormData = {
    name: string,
    email: string;
    description: string,
}

export async function createRecord(formData: IFormData) {
    try {
        const { name, email, description } = formData;
        await sql`INSERT INTO Records (Name, Email, Description) VALUES (${name}, ${email}, ${description});`;
        return { success: true }
    } catch (error) {
        console.error('Error creating record:', error);
    }
}

export async function getRecords() {
    try {
        noStore();
        const data = await sql`SELECT * FROM Records;`;
        console.log({data})
        return data;
    } catch (error) {
        console.error('Error creating record:', error);
    }
}